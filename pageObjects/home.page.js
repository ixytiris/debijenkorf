var Page = require('./page.js').Page;
var waitForPageToLoad = require('./page.js').waitForPageToLoad;

var HomePage = Object.create(Page, {
    
    /**
    *click search icon
    */
    searchIcon:{get:function()  { 
        var searchIcon = 'section.dbk-search i.dbk-icon.dbk-icon-r_search';
        browser.waitForVisible(searchIcon);
        browser.click(searchIcon); }
    },
    
    /**
    *Mobile: Click search icon
    */
    searchIconMobile:{get:function()  { 
        browser.pause(5000)
        var searchIcon = 'section.dbk-search.visible-xs.visible-sm i.dbk-icon.dbk-icon-r_search';
        browser.waitForVisible(searchIcon);
        browser.click(searchIcon); }
    },
    
     /**
     * Returns search input locator
     */
    searchInput:{get:function()  { 
        var searchInput = 'section.dbk-search input.dbk-form--field';
        browser.waitForEnabled(searchInput);
        return browser.element(searchInput); }
    },
    
     /**
     * Mobile: Returns search input locator
     */
    searchInputMobile:{get:function()  { 
        var searchInput = 'section.dbk-search.visible-xs.visible-sm input.dbk-form--field';
        browser.waitForEnabled(searchInput);
        return browser.element(searchInput); }
    },
    

     /**
     * click search result and wait for page to load
     */
    clickSearchResultItem: { get:function()  {
        var searchResult = '.dbk-result:nth-child(1) a span';
        browser.waitForExist(searchResult);
        browser.click(searchResult);
        waitForPageToLoad();
    }},


 open: { value: function() {
        Page.open.call(this, '/');
    } }

});
module.exports = HomePage
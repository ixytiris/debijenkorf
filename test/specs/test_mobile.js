var assert = require('assert');
var getTitle = require('../../pageObjects/page.js').getTitle;
var HomePage = require('../../pageObjects/home.page.js');
var ProductsListPage = require('../../pageObjects/products.list.page.js');
var ProductPage = require('../../pageObjects/product.page.js');


describe('(a) Go to de Bijenkorf ', function() {
    it('Verify home page is loaded via Checking page title', function () {
        //Open browser, go to url
        HomePage.openMobile("/");        
        assert.equal(getTitle(), 'de Bijenkorf • ontdek de wintercollectie');
    });
});

describe('(b) Search for jeans in the top bar,select it', function() {
    it('Verify search result jeans page is loaded via Checking page title', function () {
        //input jeans on search
        HomePage.searchIconMobile
        HomePage.searchInputMobile.setValue('jeans');
          
        //Click jeans item on search menu
        HomePage.clickSearchResultItem

        //Get title , verify it
        assert.equal(getTitle(), '"jeans" • de Bijenkorf');
    });
});


describe('(c) Click second product on search results', function(done) {
    it('Verify product details page is loaded via Checking page title', function (done) {
        /**
        * Click product: jeans
        **/
        ProductsListPage.clickSecondProduct

        //Get page title , verify it
        assert.equal(getTitle(), 'SuperTrash Flared Jeans high rise flared jeans met stretch • de Bijenkorf');

    });
});

describe('(d) Select an available product variant(size)', function() {
    it('Verify product variant is changed ', function () {
        //Select size is mandatory in order item to be added on basket        
        ProductPage.selectBox.selectByVisibleText('25');
        //Verify selected value
        console.log(ProductPage.selectBox.getText('option:checked'));
        assert.equal(ProductPage.selectBox.getText('option:checked'),'25')
    });
});

describe('(e) Click on ‘In winkelmand’ button   ', function() {
    it('Product is added to basket', function () {
        //Click ‘In winkelmand’ button 
        ProductPage.clickInWinkelmand
        
        //Click dismiss
        ProductPage.clickDismissNotification

        //Click basket
        ProductPage.clickBasketOnMobile
        browser.pause(2000);

        //Verify item title on basket
        assert.equal(ProductPage.getMobileBasketProductTitle[1], 'Flared Jeans high rise flared jeans met stretch');
    });
});

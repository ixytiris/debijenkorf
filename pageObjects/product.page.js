var Page = require('./page')
var waitForPageToLoad = require('./page.js').waitForPageToLoad;


var ProductPage = Object.create(Page, {
    /**
    *define elements
    */
    selectBox:{get:function() {return browser.element('.dbk-form--input');}},                   
    

    /**
     * click in Winkelmand button
     */
    clickInWinkelmand: { get:function()  {
        var button =   '.dbk-btn_justified';
        browser.waitForExist(button);
        browser.click(button);
    }},
    
    /**
     * click notification close button
     */
    clickDismissNotification: { get:function()  {
        var dismiss = '.dbk-notification--dismiss';
        browser.waitForVisible(dismiss);
        browser.click(dismiss)
    }},
    
     /**
     * click basket icon
     */
    clickBasket: { get:function()  {
        var basket = 'button=bekijk winkelmand';
        browser.waitForVisible(basket);
        browser.click(basket)
        waitForPageToLoad()
    }},

     /**
     * click Mobile basket icon
     */
    clickBasketOnMobile: { get:function()  {
        var basket = 'span.visible-xs i.dbk-icon.dbk-icon-r_bag';
        browser.waitForVisible(basket);
        browser.click(basket)
        waitForPageToLoad()
    }},
   
    
    /**
     * Returns the text of backet product title
     */
    getBasketProductTitle: {get:function() {
        var basketHeader = 'h3.dbk-heading a';
        browser.waitForExist(basketHeader);
        var text = browser.getText(basketHeader);
        console.log('Basket product Title: "'+text+'"');
        return text;        
   }},
    
     /**
     * Mobile: Returns the text of backet product title
     */
    getMobileBasketProductTitle: {get:function() {
        var basketHeader = 'div.dbk-product-info p';
        browser.waitForExist(basketHeader);
        var text = browser.getText(basketHeader);
        console.log('Basket product Title: "'+text+'"');
        return text;        
   }},
    
});
module.exports = ProductPage
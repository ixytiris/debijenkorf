var Page = require('./page.js').Page;
var waitForPageToLoad = require('./page.js').waitForPageToLoad;

var ProductsListPage = Object.create(Page, {
                 
    /**
    * click second product on product list
    * and waitFor select box of product page to display
    */
    clickSecondProduct: { get:function()  {
        var secondProduct = '.dbk-productlist--item:nth-child(2) div a';
        browser.waitForExist(secondProduct);
        browser.click(secondProduct);
        waitForPageToLoad()
    }},

});
module.exports = ProductsListPage
function Page () {
}

/**
* Wait fo page to load 
* export in order to be used to all the page-objects
**/
var waitForPageToLoad = function(){
        browser.waitUntil(function() {
                console.log('Wait for page to load...')
                return this.execute(function(){ return document.readyState === 'complete'; });
            },100000,1000);
}

/**
* Get current page title
*/
  var getTitle = function() {
        var title = browser.getTitle(); 
        console.log('Page tile is: "'+title+'"');
        return title;
   };

/**
* Open web browser
**/
Page.prototype.open = function (path) {
    browser.url(path)
    browser.windowHandleMaximize("current")
    waitForPageToLoad()
};

/**
* Open mobile browser
**/
Page.prototype.openMobile = function (path) {
    browser.url(path)
    waitForPageToLoad()
};

module.exports = {
    Page: new Page(),
    waitForPageToLoad: waitForPageToLoad,
    getTitle : getTitle
}